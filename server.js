var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());

var requestjson = require('request-json');

var path = require('path');

var urlmovimientoMlab = "https://api.mlab.com/api/1/databases/bdbbanca2/collections/movimientos?apiKey=dbUpDdVYzi0uY1kYqxXCmGFhcNn9Zg1G";

var clienteMlab=requestjson.createClient(urlmovimientoMlab);

var data = {
  "idcliente": 9999,
  "nombre": "PBM-HRL",
  "apellido": "Bacilio-Leon"
}

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res)
{
  res.sendFile(path.join(__dirname, "index.html"));
})

app.get("/movimientos", function(req, res) {
  clienteMlab.get('', function (err, resM, body) {
    if(err) {
      console.log(body);
    }
    else {
      res.send(body);
    }
  })
})

/*app.post("/movimientos", function(req, res) {
  clienteMlab.post('', data, function (err, resM, body) {
    if(err) {
      console.log(body);
    }
    else {
      res.send(body);
    }
  })
})*/

app.post("/movimientos", function(req, res) {
  clienteMlab.post('', req.body, function (err, resM, body) {
    res.send(body)
  })
})

app.get("/clientes/:idcliente", function(req, res)
{
  res.send("Aqui tiene el cliente numero:" +req.params.idcliente);
})

app.post("/",function(req,res) {
  res.send("Hemos recibido su petición actualizada");
})

app.put("/",function(req,res) {
  res.send("HEMOS RECIBIDO PETICION PUT");
})

app.delete("/",function(req,res) {
  res.send("HEMOS RECIBIDO el Delete");
})
